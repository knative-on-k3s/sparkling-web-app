# sparkling-web-app

## Build

```bash
echo ${docker_pwd} | docker login --username ${docker_user} --password-stdin

docker build -t ${docker_user}/${service} .
docker push ${docker_user}/${service}
```

## Deploy

### Create

```bash
kubectl create namespace ${namespace}
kn service create ${service} \
--namespace ${namespace} \
--env MESSAGE="I 💚 Knative" \
--env BACKGROUND_COLOR="mediumslateblue" \
--env COLOR="lightyellow" \
--image docker.io/${docker_user}/${service}
```

### Update

```bash
kn service update ${service} \
--namespace ${namespace} \
--env MESSAGE="I 💚🖐️ Knative" \
--env BACKGROUND_COLOR="crimson" \
--env COLOR="lightyellow" \
--image docker.io/${docker_user}/${service}
```
